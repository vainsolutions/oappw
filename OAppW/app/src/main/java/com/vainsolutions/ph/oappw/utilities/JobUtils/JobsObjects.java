/*
 * Copyright (c) 2015. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.vainsolutions.ph.oappw.Utilities.JobUtils;

import android.util.Log;

import com.vainsolutions.ph.oappw.Utilities.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
public class JobsObjects  {



public List<HashMap<String, String>> parse(JSONObject jObject) {

    JSONArray jMarkers = null;
    try {
        /** Retrieves all the elements in the 'markers' array */


        int success = jObject.getInt(Constant.SUCCESS);
        if (success == 1) {


            jMarkers = jObject.getJSONArray("JobSector_Tbl");


        }else{
            Log.d("Result:", "No Result");
        }

    } catch (JSONException e) {
        e.printStackTrace();
    }
    /** Invoking getMarkers with the array of json object
     * where each json object represent a marker
     */
    return getMarkers(jMarkers);
}

        private List<HashMap<String, String>> getMarkers(JSONArray jMarkers) {
            int markersCount = jMarkers.length();

            List<HashMap<String, String>> markersList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> marker = null;

            /** Taking each marker, parses and adds to list object */
            for (int i = 0; i < markersCount; i++) {
                try {
                    /** Call getMarker with marker JSON object to parse the marker */
                    marker = getMarker((JSONObject) jMarkers.get(i));
                    markersList.add(marker);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return markersList;
        }

        /**
         * Parsing the Marker JSON object
         */
        private HashMap<String, String> getMarker(JSONObject jMarker) {
            HashMap<String, String> marker = new HashMap<String, String>();


            String hname        = "-NA-";
            String haddress     = "-NA-";
            String hcontact     = "-NA-";
            String htype        = "-NA-";
            String hhealthcard  = "-NA-";
            String hspecial     = "-NA-";
            String hlat         = "-NA-";
            String hlong        = "-NA-";

            String hsector          = "-NA-";
            String hdate            = "-NA-";
            String hthumbimg        = "-NA-";

            try {
                // Extracting formatted address, if available
                if (!jMarker.isNull("jobTitle")) {
                    hname = jMarker.getString("jobTitle");
                }
                haddress    = jMarker.getString("jobDesc");
                hcontact    = jMarker.getString("jobSalary");
                htype       = jMarker.getString("jobAvailable");
                hhealthcard = jMarker.getString("jobCountry");
                hspecial    = jMarker.getString("jobLong");
                hlat        = jMarker.getString("hosLat");
                hlong       = jMarker.getString("hosLong");
                hsector     = jMarker.getString("jobSector");
                hdate       = jMarker.getString("date_added");
                hthumbimg       = jMarker.getString("jobThumbImg");





                marker.put("jobTitle", hname);
                marker.put("jobDesc", haddress);
                marker.put("jobSalary", hcontact);
                marker.put("jobAvailable", htype);
                marker.put("jobCountry", hhealthcard);
                marker.put("jobLong", hspecial);
                marker.put("hosLat", hlat);
                marker.put("hosLong", hlong);
                marker.put("jobSector", hsector);
                marker.put("date_added", hdate);
                marker.put("jobThumbImg", hthumbimg);

            } catch (JSONException e) {
                e.printStackTrace();
            }


            return marker;
        }
}
