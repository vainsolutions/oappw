/*
 * Copyright (c) 2015. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.vainsolutions.ph.oappw.Activities.IntroPager;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.vainsolutions.ph.oappw.Activities.IntroPager.FragmentPage.SlideFourFragment;
import com.vainsolutions.ph.oappw.Activities.IntroPager.FragmentPage.SlideOneFragment;
import com.vainsolutions.ph.oappw.Activities.IntroPager.FragmentPage.SlideThreeFragment;
import com.vainsolutions.ph.oappw.Activities.IntroPager.FragmentPage.SlideTwoFragment;

public class IntroViewPagerAdapter extends FragmentPagerAdapter {
    public static Context _context;
    public static int totalPage = 4;

    public IntroViewPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        _context = context;

    }

    @Override
    public Fragment getItem(int position) {
        Fragment f = new Fragment();
        switch (position) {
            case 0:
                f = SlideOneFragment.newInstance(_context);
                break;
            case 1:
                f = SlideTwoFragment.newInstance(_context);
                break;
            case 2:
                f= SlideThreeFragment.newInstance(_context);
                break;
            case 3:
                f= SlideFourFragment.newInstance(_context);
                break;

        }
        return f;
    }

    @Override
    public int getCount() {
        return totalPage;
    }

}