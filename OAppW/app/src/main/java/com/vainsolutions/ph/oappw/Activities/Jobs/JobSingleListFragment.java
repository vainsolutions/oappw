/*
 * Copyright (c) 2015. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.vainsolutions.ph.oappw.Activities.Jobs;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.vainsolutions.ph.oappw.R;
import com.vainsolutions.ph.oappw.Utilities.Constant;
import com.vainsolutions.ph.oappw.Utilities.HTTP.JSONParser;
import com.vainsolutions.ph.oappw.Utilities.JobUtils.JobsObjects;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class JobSingleListFragment extends ActionBarActivity {
    // Declare Variables
    JSONObject jsonobject;
    JSONArray jsonarray;
    ListView listview;
    JobsListViewAdapter adapter;
    ProgressDialog mProgressDialog;
    ArrayList<HashMap<String, String>> arraylist;
    // Creating JSON Parser object
    JSONParser jParser = new JSONParser();//for get all posts
    public static final String MyPREFERENCES = "Files";
    SharedPreferences sp;
    JSONParser jsonParser = new JSONParser();

    TextView marqueetext;
    private ProgressDialog progressDialog;
    private ProgressDialog pDialog;

    public static final String TAG_SUCCESS = "success";
    public static final String TAG_JOBUID = "jobID";
    public static final String TAG_JOBTITLE = "jobTitle";
    public static final String TAG_JOBDESC = "jobDesc";
    public static final String TAG_JOBSALARY = "jobSalary";
    public static final String TAG_JOBAVAILABLE = "jobAvailable";
    public static final String TAG_JOBCOUNTRY = "jobCountry";
    public static final String TAG_JOBLONG = "jobLong";
    public static final String TAG_JOBLAT = "jobLat";
    public static final String TAG_JOBSECTOR = "jobSector";
    public static final String TAG_JOBIMAGE = "jobThumbImg";
    public static final String TAG_JOBDATEADDED = "date_added";

    JobsObjects markerParser;
    String viaSector;
    String[] stringarray;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.themecolor)));
        // enabling action bar app icon and behaving it as toggle button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setContentView(R.layout.job_list);
        marqueetext = (TextView) findViewById(R.id.marqueetext);
        marqueetext.setText("OFW - Mga Bagong Bayani • OFW - Mga Bagong Bayani •");
        marqueetext.setSelected(true);
        marqueetext.setTypeface(null, Typeface.BOLD);
        marqueetext.setSingleLine();
        marqueetext.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        marqueetext.setHorizontallyScrolling(true);

        Intent i = getIntent();
        viaSector = i.getStringExtra("findbySector");

//
        new DownloadJSON().execute();
    }


    private class RetrieveBySector extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(JobSingleListFragment.this);
            pDialog.setMessage("Searching...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(Void... params) {
            String buildingString = viaSector;
            Log.d("Sector:", buildingString);
            arraylist = new ArrayList<HashMap<String, String>>();
            List<NameValuePair> parameter = new ArrayList<NameValuePair>();
            parameter.add(new BasicNameValuePair("jobSector", buildingString));

            jsonobject = jsonParser.makeHttpRequest(Constant.GETALLJOB_SECTOR,
                    "POST", parameter);
//
            try {
                // Locate the array name in JSON
                int success = jsonobject.getInt(TAG_SUCCESS);
                if (success == 1) {
                    jsonarray = jsonobject.getJSONArray("JobSector_Tbl");

                    for (int i = 0; i < jsonarray.length(); i++) {
                        jsonobject = jsonarray.getJSONObject(i);
                        HashMap<String, String> map = new HashMap<String, String>();
                        // Retrive JSON Objects

                        map.put("jobID", jsonobject.getString(TAG_JOBUID));
                        map.put("jobTitle", jsonobject.getString(TAG_JOBTITLE));
                        map.put("jobDesc", jsonobject.getString(TAG_JOBDESC));
                        map.put("jobSalary", jsonobject.getString(TAG_JOBSALARY));

                        map.put("jobAvailable", jsonobject.getString(TAG_JOBAVAILABLE));
                        map.put("jobCountry", jsonobject.getString(TAG_JOBCOUNTRY));
                        map.put("jobLong", jsonobject.getString(TAG_JOBLONG));
                        map.put("jobLat", jsonobject.getString(TAG_JOBLAT));
                        map.put("jobSector", jsonobject.getString(TAG_JOBSECTOR));
                        map.put("jobThumbImg", jsonobject.getString(TAG_JOBIMAGE));
                        map.put("date_added", jsonobject.getString(TAG_JOBDATEADDED));

                        arraylist.add(map);

                    }

                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            return jsonobject.toString();

        }

        protected void onPostExecute(Void args) {
            listview = (ListView) findViewById(R.id.jobslist);
            // Pass the results into ListViewAdapter.java
            adapter = new JobsListViewAdapter(JobSingleListFragment.this, arraylist);
            // Set the adapter to the ListView
            listview.setAdapter(adapter);
            // Close the progressdialog
            mProgressDialog.dismiss();


        }

    }


    private class DownloadJSON extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Create a progressdialog
            mProgressDialog = new ProgressDialog(JobSingleListFragment.this);
            // Set progressdialog title
            mProgressDialog.setTitle("OAppW");
            // Set progressdialog message
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(false);
            // Show progressdialog
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // Create an array
            arraylist = new ArrayList<HashMap<String, String>>();
            // Retrieve JSON Objects from the given URL address
//            jsonobject = JSONfunctions
//                    .getJSONfromURL(Constant.GETALLJOB_SECTOR);

            String buildingString = viaSector;
            Log.d("Sector:", buildingString);

            List<NameValuePair> parameter = new ArrayList<NameValuePair>();
            parameter.add(new BasicNameValuePair("jobSector", buildingString));

            jsonobject = jsonParser.makeHttpRequest(Constant.GETALLJOB_SECTOR,
                    "POST", parameter);

            try {
                // Locate the array name in JSON
                int success = jsonobject.getInt(TAG_SUCCESS);
                if (success == 1) {
                    jsonarray = jsonobject.getJSONArray("JobSector_Tbl");

                    for (int i = 0; i < jsonarray.length(); i++) {
                        jsonobject = jsonarray.getJSONObject(i);
                        HashMap<String, String> map = new HashMap<String, String>();
                        // Retrive JSON Objects

                        map.put("jobID", jsonobject.getString(TAG_JOBUID));
                        map.put("jobTitle", jsonobject.getString(TAG_JOBTITLE));
                        map.put("jobDesc", jsonobject.getString(TAG_JOBDESC));
                        map.put("jobSalary", jsonobject.getString(TAG_JOBSALARY));

                        map.put("jobAvailable", jsonobject.getString(TAG_JOBAVAILABLE));
                        map.put("jobCountry", jsonobject.getString(TAG_JOBCOUNTRY));
                        map.put("jobLong", jsonobject.getString(TAG_JOBLONG));
                        map.put("jobLat", jsonobject.getString(TAG_JOBLAT));
                        map.put("jobSector", jsonobject.getString(TAG_JOBSECTOR));
                        map.put("jobThumbImg", jsonobject.getString(TAG_JOBIMAGE));
                        map.put("date_added", jsonobject.getString(TAG_JOBDATEADDED));

                        arraylist.add(map);

                    }

                }
            } catch (JSONException e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            listview = (ListView) findViewById(R.id.jobslist);
            // Pass the results into ListViewAdapter.java
            adapter = new JobsListViewAdapter(JobSingleListFragment.this, arraylist);
            // Set the adapter to the ListView
            listview.setAdapter(adapter);
            // Close the progressdialog
            mProgressDialog.dismiss();


        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                // app icon in action bar clicked; go home
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
    }

}