package com.vainsolutions.ph.oappw.Activities.IntroPager;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.widget.Button;

import com.vainsolutions.ph.oappw.R;


public class IntroPagerActivity extends FragmentActivity {
    private ViewPager _mViewPager;
    private IntroViewPagerAdapter _adapter;
    private Button _btn1, _btn2, _btn3, _btn4;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpager_main);
        setUpView();
        setTab();
    }

    private void setUpView() {
        _mViewPager = (ViewPager) findViewById(R.id.viewPager);
        _adapter = new IntroViewPagerAdapter(getApplicationContext(), getSupportFragmentManager());
        _mViewPager.setAdapter(_adapter);
        _mViewPager.setCurrentItem(0);
        initButton();
    }

    private void setTab() {
        _mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrollStateChanged(int position) {

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageSelected(int position) {
                // TODO Auto-generated method stub
                btnAction(position);
            }

        });
    }

    private void btnAction(int action) {
        switch (action) {
            case 0:
                setButton(_btn1, "", 15, 15);
                setButton(_btn2, "", 5, 5);
                break;
            case 1:
                setButton(_btn2, "", 15, 15);
                setButton(_btn1, "", 5, 5);
                setButton(_btn3, "", 5, 5);
                break;
            case 2:
                setButton(_btn3, "", 15, 15);
                setButton(_btn2, "", 5, 5);
                setButton(_btn4, "", 5, 5);
                break;
            case 3:
                setButton(_btn4, "", 15, 15);
                setButton(_btn3, "", 5, 5);
                break;
        }
    }

    private void initButton() {
        _btn1 = (Button) findViewById(R.id.btn1);
        _btn2 = (Button) findViewById(R.id.btn2);
        _btn3 = (Button) findViewById(R.id.btn3);
        _btn4 = (Button) findViewById(R.id.btn4);
        setButton(_btn1, "", 15, 15);
        setButton(_btn2, "", 5, 5);
        setButton(_btn4, "", 5, 5);
        setButton(_btn3, "", 5, 5);
    }

    private void setButton(Button btn, String text, int h, int w) {
        btn.setWidth(w);
        btn.setHeight(h);
        btn.setText(text);
    }
}