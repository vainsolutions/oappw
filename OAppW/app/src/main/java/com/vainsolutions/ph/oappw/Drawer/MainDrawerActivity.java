package com.vainsolutions.ph.oappw.Drawer;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;
import com.vainsolutions.ph.oappw.Activities.Fragments.FeedbackFragment;
import com.vainsolutions.ph.oappw.Activities.HomeFragmentActivity;
import com.vainsolutions.ph.oappw.Activities.Jobs.JobMainCategoryActivity;
import com.vainsolutions.ph.oappw.Activities.Map.HereMapMainActivity;
import com.vainsolutions.ph.oappw.Activities.Reports.AddReports;
import com.vainsolutions.ph.oappw.Activities.Reports.GetReports;
import com.vainsolutions.ph.oappw.R;
import com.vainsolutions.ph.oappw.Utilities.AlertDialogManager;
import com.vainsolutions.ph.oappw.Utilities.ConnectionDetector;
import com.vainsolutions.ph.oappw.Utilities.RateThisApp;

import java.util.ArrayList;
import java.util.List;

public class MainDrawerActivity extends ActionBarActivity {
    String[] menutitles;
    TypedArray menuIcons;
    AsyncTask<Void, Void, Void> mRegisterTask;
    // nav drawer title
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;

    private DrawerLayout mDrawerLayout;
    private LinearLayout mDrawerLinear;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private List<RowItem> rowItems;
    private CustomAdapter adapter;
    // Alert dialog manager
    AlertDialogManager alert = new AlertDialogManager();
    SharedPreferences.Editor ed;
    // Connection detector
    ConnectionDetector cd;
    private FrameLayout frame;
    private float lastTranslate = 0.0f;
    public static String name;
    public static String randomimage;
    String uimg;
    ProfilePictureView ofwfbdp;

    TextView userwelcometext;
    SharedPreferences sp;
    public static String file = "Files";
    AlertDialog.Builder alertDialog;
    private int Imgid = R.drawable.blue_bar;
    private UiLifecycleHelper uiHelper;

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_layout);
        sp = getSharedPreferences(file, 0);
        Typeface OmnesLight = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Thin.ttf");
        cd = new ConnectionDetector(getApplicationContext());


        try {
            SharedPreferences.Editor ed = sp.edit();
            ed.putString("isfirstlunch", "1");
            ed.commit();
        } catch (Exception e) {
            e.printStackTrace();

        }


        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(MainDrawerActivity.this,
                    "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.themecolor)));
        // enabling action bar app icon and behaving it as toggle button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
//        SlideFourFragment fourFragment = new SlideFourFragment();
        mTitle = mDrawerTitle = getTitle();

        uiHelper = new UiLifecycleHelper(MainDrawerActivity.this, callback);
        uiHelper.onCreate(savedInstanceState);

        menutitles = getResources().getStringArray(R.array.titles);
        menuIcons = getResources().obtainTypedArray(R.array.icons);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.slider_list);
        mDrawerLinear = (LinearLayout) findViewById(R.id.sideMenu);
        frame = (FrameLayout) findViewById(R.id.frame_container);
        ofwfbdp = (ProfilePictureView) findViewById(R.id.ofwfbdp);
        ofwfbdp.setCropped(true);

        userwelcometext = (TextView) findViewById(R.id.userwelcometext);

        userwelcometext.setTypeface(OmnesLight);
        ed = sp.edit();
        name = sp.getString("storedname", "");
        userwelcometext.setText("Hi " + name + "!" + "\n ");


        // Check for an open FACEBOOK session
        Session session = Session.getActiveSession();
        if (session != null && session.isOpened()) {
            // Get the user's data
            makeMeRequest(session);
        } else {
            ofwfbdp.setVisibility(View.GONE);
        }

        DrawerLayout.LayoutParams params = (android.support.v4.widget.DrawerLayout.LayoutParams) mDrawerLinear.getLayoutParams();
        mDrawerLinear.setLayoutParams(params);

        rowItems = new ArrayList<RowItem>();

        for (int i = 0; i < menutitles.length; i++) {
            RowItem items = new RowItem(menutitles[i], menuIcons.getResourceId(
                    i, -1));
            rowItems.add(items);
        }

        menuIcons.recycle();

        adapter = new CustomAdapter(getApplicationContext(), rowItems);

        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new SlideitemListener());


        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, R.string.app_name, R.string.app_name) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }

            @SuppressLint("NewApi")
            public void onDrawerSlide(View drawerView, float slideOffset) {
                float moveFactor = (mDrawerLinear.getWidth() * slideOffset);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    frame.setTranslationX(moveFactor);
                } else {
                    TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                    anim.setDuration(0);
                    anim.setFillAfter(true);
                    frame.startAnimation(anim);

                    lastTranslate = moveFactor;
                }
            }

        };


        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            // on first time display view for first nav item
            updateDisplay(0);
        }
    }

    class SlideitemListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            updateDisplay(position);

        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void updateDisplay(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0://home
                fragment = new HomeFragmentActivity();
                break;
            case 1:
                Intent jobDetail = new Intent(getApplicationContext(),  JobMainCategoryActivity.class);
                startActivity(jobDetail);
                break;
            case 2:

                Intent addreports = new Intent(getApplicationContext(),  AddReports.class);
                startActivity(addreports);

                break;
            case 3:
                Intent getreports = new Intent(getApplicationContext(),  GetReports.class);
                startActivity(getreports);

                break;
            case 4:

                Intent toMap = new Intent(getApplicationContext(), HereMapMainActivity.class);
                startActivity(toMap);
                break;

            case 5:

                fragment = new FeedbackFragment();

                break;
            case 6://About
//                fragment = new AboutFragments();
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                }


                break;

            case 7:

                finish();
                break;

            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();

            fragmentManager.popBackStack();
            // update selected item and title, then close the drawer
            setTitle(menutitles[position]);
            mDrawerLayout.closeDrawer(mDrawerLinear);
        } else {
            // error in creating fragment
            Log.e("MainDrawerActivity", "Error in creating fragment");
        }

    }


    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
//        getActionBar().setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
             /*switch (item.getItemId()) {
                    case : R.id.action_settings:
                                  return true;
                    default :

                }*/
        return super.onOptionsItemSelected(item);
    }

    /**
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerLinear);
        menu.findItem(R.id.action_add).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void onBackPressed() {


        // See bug: http://stackoverflow.com/questions/13418436/android-4-2-back-stack-behaviour-with-nested-fragments/14030872#14030872
        // If the fragment exists and has some back-stack entry
        FragmentManager fm = getFragmentManager();
        Fragment currentFragment = fm.findFragmentById(R.id.frame_container);
        if (currentFragment != null && currentFragment.getChildFragmentManager().getBackStackEntryCount() > 0) {
            // Get the fragment fragment manager - and pop the backstack
            currentFragment.getChildFragmentManager().popBackStack();
        }
        // Else, nothing in the direct fragment back stack
        else {
//            if (currentFragment.getTag() != null) {
//                finish();
//            } else {
//                super.onBackPressed();
//            }
        }
    }

    public void finish() {

        final Dialog dialog = new Dialog(MainDrawerActivity.this,
                R.style.DialogAnim);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.validate_message);

        Button bOk = (Button) dialog.findViewById(R.id.buttonOk);
        Button bExit = (Button) dialog.findViewById(R.id.buttonExit);
        Button bCancel = (Button) dialog.findViewById(R.id.buttonCancel);
        TextView question = (TextView) dialog.findViewById(R.id.tvalertmessage);

        question.setText("Are you sure? You want to Logout your account?");
        bOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (Session.getActiveSession() != null) {
                    Session.getActiveSession().closeAndClearTokenInformation();
                    MainDrawerActivity.this.finish();
                    Session.setActiveSession(null);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(0);

                }

                MainDrawerActivity.this.finish();
                sp = getSharedPreferences(file, 0);
                SharedPreferences.Editor ed = sp.edit();
                ed.clear();
                ed.apply();
                ed.commit();

                Session.setActiveSession(null);
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);

                dialog.dismiss();
            }
        });

        bExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(0);
                quit();
            }
        });

        bCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public void quit() {
        super.finish();
    }

    private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
//        if (session != null && session.isOpened()) {
        if (state.isOpened()) {
            // Get the user's data.
            makeMeRequest(session);
        } else {

        }
    }

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(final Session session, final SessionState state, final Exception exception) {
            onSessionStateChange(session, state, exception);

        }
    };

    private void makeMeRequest(final Session session) {
        // Make an API call to get user data and define a
        // new callback to handle the response.
        Request request = Request.newMeRequest(session,
                new Request.GraphUserCallback() {
                    @Override
                    public void onCompleted(GraphUser user, Response response) {
                        // If the response is successful
                        if (session == Session.getActiveSession()) {
                            if (user != null) {
                                // Set the id for the ProfilePictureView
                                // view that in turn displays the profile picture.
                                ofwfbdp.setProfileId(user.getId());
                                // Set the Textview's text to the user's name.
                                String gender = user.asMap().get("gender").toString();
                                if (gender.equalsIgnoreCase("Male")) {
                                    userwelcometext.setText("Hi " + user.getName());
                                } else if (gender.equalsIgnoreCase("Female")) {
                                    userwelcometext.setText("Hi " + user.getName());
                                }
                            } else {
                                ofwfbdp.setVisibility(View.VISIBLE);
                            }
                        }
                        if (response.getError() != null) {
                            // Handle errors, will do so later.
                        }
                    }
                });
        request.executeAsync();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Monitor launch times and interval from installation
        RateThisApp.onStart(this);
        // Show a dialog if criteria is satisfied
        RateThisApp.showRateDialogIfNeeded(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        uiHelper.onSaveInstanceState(bundle);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

}