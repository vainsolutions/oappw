/*
 * Copyright (c) 2015. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.vainsolutions.ph.oappw.Activities.Reports;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.vainsolutions.ph.oappw.R;
import com.vainsolutions.ph.oappw.Utilities.Constant;
import com.vainsolutions.ph.oappw.Utilities.HTTP.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class GetReports extends ListActivity {

    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    JSONParser jParser = new JSONParser();

    ArrayList<HashMap<String, String>> reportList;

    // url to get all reports list


    private static final String TAG_SUCCESS = "success";
    private static final String TAG_REPORTS = "reports";
    private static final String TAG_RID = "rid";
    private static final String TAG_REPORTER = "reporter";

    private static final String TAG_REPORT_TYPE = "report_type";
    private static final String TAG_COUNTRY = "country";
    private static final String TAG_REPORT_DATE = "report_date";
    private static final String TAG_REPORT_TIME = "report_time";
    private static final String TAG_LONG = "long";
    private static final String TAG_LAT = "lat";
    private static final String TAG_RECEIVER_NAME = "receiver_name";
    private static final String TAG_RECEIVER_NUMBER = "receiver_number";
    private static final String TAG_REPORT_DETAILS = "report_details";
    private static final String TAG_REPORT_IMAGE = "report_image";

    String rid = "", reporter = "";
    String report_type = "", country = "", report_date = "", report_time = "", lon = "", lat = "", receiver_name = "", receiver_number = "", report_details = "", report_image = "";
    String filter = "all";
    String qry = "where report_type = \"abuse\" and country = \"ph\"";
    // reports JSONArray
    JSONArray reports = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.getreports_layout);

        // Hashmap for ListView
        reportList = new ArrayList<HashMap<String, String>>();

        // Loading reports in Background Thread
//		new LoadReports().execute();
        dialog_box();
        // Get listview
        ListView lv = getListView();

        // on seleting single product
        // launching Edit Product Screen
        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // getting values from selected ListItem
                String rid = ((TextView) view.findViewById(R.id.rid)).getText()
                        .toString();
//
//				Intent in = new Intent(getApplicationContext(),
//						OtherActivity.class);
//				in.putExtra(TAG_RID, rid);
//
//				startActivityForResult(in, 100);
            }
        });

    }

    public void dialog_box() {

        new LoadReports().execute();
    }


    class LoadReports extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(GetReports.this);
            pDialog.setMessage("Loading reports. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        protected String doInBackground(String... args) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("filter", filter));
            params.add(new BasicNameValuePair("qry", qry));
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(Constant.url_get_reports, "POST", params);

            // Check your log cat for JSON reponse
            Log.d("All Reports: ", json.toString());

            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // reports found
                    // Getting Array of Reports
                    reports = json.getJSONArray(TAG_REPORTS);

                    // looping through All Reports
                    for (int i = 0; i < reports.length(); i++) {
                        JSONObject c = reports.getJSONObject(i);

                        // Storing each json item in variable
                        rid = c.getString(TAG_RID);
                        reporter = c.getString(TAG_REPORTER);

                        report_type = c.getString(TAG_REPORT_TYPE);
                        country = c.getString(TAG_COUNTRY);
                        report_date = c.getString(TAG_REPORT_DATE);
                        report_time = c.getString(TAG_REPORT_TIME);
                        lon = c.getString(TAG_LONG);
                        lat = c.getString(TAG_LAT);
                        receiver_name = c.getString(TAG_RECEIVER_NAME);
                        receiver_number = c.getString(TAG_RECEIVER_NUMBER);
                        report_details = c.getString(TAG_REPORT_DETAILS);
                        report_image = c.getString(TAG_REPORT_IMAGE);

                        // creating new HashMap
                        HashMap<String, String> map = new HashMap<String, String>();

                        // adding each child node to HashMap key => value
                        map.put(TAG_RID, rid);
                        map.put(TAG_REPORTER, reporter);

                        map.put(TAG_REPORT_TYPE, report_type);
                        map.put(TAG_COUNTRY, country);
                        map.put(TAG_REPORT_DATE, report_date);
                        map.put(TAG_REPORT_TIME, report_time);
                        map.put(TAG_LONG, lon);
                        map.put(TAG_LAT, lat);
                        map.put(TAG_RECEIVER_NAME, receiver_name);
                        map.put(TAG_RECEIVER_NUMBER, receiver_number);
                        map.put(TAG_REPORT_DETAILS, report_details);
                        map.put(TAG_REPORT_IMAGE, report_image);

                        // adding HashList to ArrayList
                        reportList.add(map);
                    }
                } else {
                    // no reports found
                    Toast.makeText(getApplicationContext(), "No Reports Found", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all reports
            pDialog.dismiss();
            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ListAdapter adapter = new SimpleAdapter(
                            GetReports.this, reportList,
                            R.layout.list_item_reports, new String[]{TAG_RID,
                            TAG_REPORTER,
                            TAG_REPORT_TYPE,
                            TAG_COUNTRY,
                            TAG_REPORT_DATE,
                            TAG_REPORT_TIME,
                            TAG_LONG,
                            TAG_LAT,
                            TAG_RECEIVER_NAME,
                            TAG_RECEIVER_NUMBER,
                            TAG_REPORT_DETAILS,
                            TAG_REPORT_IMAGE},
                            new int[]{R.id.rid, R.id.reporter,
                                    R.id.report_type,
                                    R.id.country,
                                    R.id.date,
                                    R.id.time,
                                    R.id.lon,
                                    R.id.lat,
                                    R.id.receiver_name,
                                    R.id.receiver_number,
                                    R.id.report_details,
                                    R.id.report_image});
                    // updating listview
                    setListAdapter(adapter);
                }
            });

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                // app icon in action bar clicked; go home
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
