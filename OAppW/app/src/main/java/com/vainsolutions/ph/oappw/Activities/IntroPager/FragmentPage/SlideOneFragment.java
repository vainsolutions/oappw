/*
 * Copyright (c) 2015. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.vainsolutions.ph.oappw.Activities.IntroPager.FragmentPage;


import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vainsolutions.ph.oappw.R;

/**
 * Created by TIP Student on 4/11/15.
 * BRV OAppW
 */
public class SlideOneFragment  extends Fragment {

    TextView shoetextpager;
    public static Fragment newInstance(Context context) {
        SlideOneFragment f = new SlideOneFragment();

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.slide_layout_one, null);

        ImageView imagesliderid = (ImageView) root.findViewById(R.id.imagesliderid);
//        imagesliderid.setBackgroundResource(R.drawable.slideimageone);

        shoetextpager = (TextView) root.findViewById(R.id.slideonetextdesc);
        shoetextpager.setText("One");
        Typeface robotothin = Typeface.createFromAsset(getActivity().getAssets(), "fonts/silom.ttf");
        shoetextpager.setTypeface(robotothin);
        return root;
    }

}
