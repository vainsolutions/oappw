

package com.vainsolutions.ph.oappw.Utilities;
public class ListViewItem {
    public final String title;        // the text for the ListView item title

    public ListViewItem(String title) {

        this.title = title;
    }
}