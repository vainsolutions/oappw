

package com.vainsolutions.ph.oappw.Activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.vainsolutions.ph.oappw.R;

import java.util.HashMap;
public class HomeFragmentActivity extends Fragment implements BaseSliderView.OnSliderClickListener {
    ProgressBar pb;
    Dialog dialog;
    int downloadedSize = 0;
    int totalSize = 0;
    TextView cur_val;

    private static final int DURATION = 400;
    View mFrontView;
    View mBackView;

    private View parentView;
    private SliderLayout mDemoSlider;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.home_layout, container, false);
        setUpViews();



        return parentView;
    }

    public void setUpViews(){
        mDemoSlider = (SliderLayout)parentView.findViewById(R.id.sliderbusiness);

        HashMap<String,String> url_maps = new HashMap<String, String>();
        url_maps.put("", "OFW");
        url_maps.put("", "OFW");
        url_maps.put("", "OFW");
        url_maps.put("", "OFW");
        url_maps.put("", "OFW");
        url_maps.put("", "OFW");

        HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("OFW ",R.drawable.sectoraccounting);
        file_maps.put("",R.drawable.sectorhealth);
        file_maps.put("", R.drawable.sectormanufacture);
        file_maps.put("", R.drawable.sectorengineer);
        file_maps.put("", R.drawable.sectorscience);

        for(String name : file_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(HomeFragmentActivity.this);

            //add your extra information
            textSliderView.getBundle()
                    .putString("extra",name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(12000);


    }

    public void onSliderClick(BaseSliderView slider) {
        Toast.makeText(getActivity(), slider.getBundle().get("extra") + "", Toast.LENGTH_SHORT).show();
    }



    void showError(final String err){
        getActivity().runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(getActivity(), err, Toast.LENGTH_LONG).show();
            }
        });
    }



    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
    public void flip(final View front, final View back, final int duration) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            AnimatorSet set = new AnimatorSet();
            set.playSequentially(
                    ObjectAnimator.ofFloat(front, "rotationY", 90).setDuration(duration / 2),
                    ObjectAnimator.ofInt(front, "visibility", View.VISIBLE).setDuration(0),
                    ObjectAnimator.ofFloat(back, "rotationY", -90).setDuration(0),
                    ObjectAnimator.ofInt(back, "visibility", View.GONE).setDuration(0),
                    ObjectAnimator.ofFloat(back, "rotationY", 0).setDuration(duration / 2)
            );
            set.start();
        }
        else {
            front.animate().rotationY(90).setDuration(duration / 2).setListener(new AnimatorListenerAdapter() {

                @Override
                public void onAnimationEnd(Animator animation) {
                    front.setVisibility(View.VISIBLE);
                    back.setRotationY(-90);
                    back.setVisibility(View.VISIBLE);
                    back.animate().rotationY(0).setDuration(duration / 2).setListener(null);
                }
            });
        }



    }

}
