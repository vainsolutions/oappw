package com.vainsolutions.ph.oappw.Activities.Jobs;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vainsolutions.ph.oappw.R;
import com.vainsolutions.ph.oappw.Utilities.ImageLoader.ImageLoader;

import java.util.ArrayList;
import java.util.HashMap;
public class JobsListViewAdapter extends BaseAdapter {

    // Declare Variables
    Context context;
    LayoutInflater inflater;
    ArrayList<HashMap<String, String>> data;
    ImageLoader imageLoader;
    HashMap<String, String> resultp = new HashMap<String, String>();
    public static final String MyPREFERENCES = "Files";
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    public JobsListViewAdapter(Context context,
                                     ArrayList<HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
        imageLoader = new ImageLoader(context);

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        // Declare Variables
        TextView joblistname, joblistDesc;
        ImageView jobsdpid;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View itemView = inflater.inflate(R.layout.jobs_singlelist, parent, false);
        // Get the position
        resultp = data.get(position);

        // Locate the TextViews in listview_item.xml
        joblistname = (TextView) itemView.findViewById(R.id.joblistname);
        joblistDesc = (TextView) itemView.findViewById(R.id.joblistdescription);


        // Locate the ImageView in listview_item.xml
        jobsdpid = (ImageView) itemView.findViewById(R.id.jobsdpid);

        sp = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        ed = sp.edit();

        joblistname.setText(resultp.get(JobSingleListFragment.TAG_JOBTITLE));
        joblistDesc.setText(resultp.get(JobSingleListFragment.TAG_JOBDESC));

        int loader = 1;
        imageLoader.DisplayImage(resultp.get(JobSingleListFragment.TAG_JOBIMAGE), loader, jobsdpid);

        // Capture ListView item click
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // Get the position
                resultp = data.get(position);

                Intent intent = new Intent(context, JobSingleDetailView.class);
                // Pass all data

                intent.putExtra("jobID", resultp.get(JobSingleListFragment.TAG_JOBUID));
                intent.putExtra("jobTitle", resultp.get(JobSingleListFragment.TAG_JOBTITLE));
                intent.putExtra("jobDesc", resultp.get(JobSingleListFragment.TAG_JOBDESC));
                intent.putExtra("jobSalary", resultp.get(JobSingleListFragment.TAG_JOBSALARY));
                intent.putExtra("jobAvailable", resultp.get(JobSingleListFragment.TAG_JOBAVAILABLE));

                intent.putExtra("jobCountry", resultp.get(JobSingleListFragment.TAG_JOBCOUNTRY));
                intent.putExtra("jobLong", resultp.get(JobSingleListFragment.TAG_JOBLONG));

                intent.putExtra("jobLat", resultp.get(JobSingleListFragment.TAG_JOBLAT));
                intent.putExtra("jobSector", resultp.get(JobSingleListFragment.TAG_JOBSECTOR));
                intent.putExtra("jobThumbImg", resultp.get(JobSingleListFragment.TAG_JOBIMAGE));
                intent.putExtra("date_added", resultp.get(JobSingleListFragment.TAG_JOBDATEADDED));

                // Pass all data flag
//                intent.putExtra("inaanak_image", resultp.get(ViewRestaurantListFragment.TAG_RESTAUID));
                // Start SingleItemView Class
                context.startActivity(intent);

            }
        });
        return itemView;
    }
}