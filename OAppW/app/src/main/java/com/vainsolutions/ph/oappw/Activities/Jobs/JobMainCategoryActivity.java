
package com.vainsolutions.ph.oappw.Activities.Jobs;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.vainsolutions.ph.oappw.R;
import com.vainsolutions.ph.oappw.Utilities.ListViewItem;

import java.util.List;

public class JobMainCategoryActivity extends ActionBarActivity {

    private List<ListViewItem> mItems;        // ListView items list

    // List view
    private ListView lv;
    // Listview Adapter
    ArrayAdapter<String> adapter;

    // ArrayList for Listview
    String jobListTitle[] = {
            "Accounting / Finance",
            "Admin/Human Resources",
            "Arts Media and Communication",
            "Building and Construction",
            "Computer - Information Technology",
            "Education and Training",
            "Engineering",
            "Healthcare",
            "Hotel and Restaurant",
            "Manufacturing",
            "Sales and Marketing",
            "Sciences",
            "Services",
    };

    Integer[] imagesRow = {
            R.drawable.sectoraccounting,
            R.drawable.sectorhr,
            R.drawable.sectormascomm,
            R.drawable.sectorconstruct,
            R.drawable.sectorit,
            R.drawable.sectorteacher,
            R.drawable.sectorengineer,
            R.drawable.sectorhealth,
            R.drawable.sectorresto,
            R.drawable.sectormanufacture,
            R.drawable.sectorsales,
            R.drawable.sectorscience,
            R.drawable.sectorservice
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.themecolor)));
        // enabling action bar app icon and behaving it as toggle button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setContentView(R.layout.jobs_list_view_main);

        JobsCustomListMenu adapter = new
                JobsCustomListMenu(JobMainCategoryActivity.this, jobListTitle, imagesRow);

        lv = (ListView) findViewById(R.id.jobs_list_main_menu);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                int itemPosition = position;
                String itemValue = (String) lv.getItemAtPosition(position);
                Intent toNumber = new Intent(JobMainCategoryActivity.this, JobSingleListFragment.class);
                toNumber.putExtra("findbySector", itemValue);
                startActivity(toNumber);

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                // app icon in action bar clicked; go home
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onBackPressed() {
        super.onBackPressed();
    }

}
