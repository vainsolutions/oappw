/*
 * Copyright (c) 2015. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.vainsolutions.ph.oappw.Utilities;

public final class Constant {
    public static final String OAppW_URL                = "http://oappw.byethost7.com";
    public static final String SUCCESS                  = "success";
    public static final String JOBSTREET_API            = "http://job-search.jobstreet.com.ph/philippines/job-opening.php?key=";
    public static final String GETALLJOB_SECTOR         = "http://oappw.byethost7.com/getalljob.php";
    public static final String FEEDBACK_URL             = "http://oappw.byethost7.com/sendfeedback.php";
    public static final String url_create_report        = "http://oappw.byethost7.com/create_report.php";
    public static final String url_sendsms              = "http://oappw.byethost7.com/smssender.php";
    public static final String url_get_reports          = "http://oappw.byethost7.com/get_reports.php";
}
