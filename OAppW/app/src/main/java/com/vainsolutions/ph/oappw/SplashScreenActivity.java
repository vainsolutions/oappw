

package com.vainsolutions.ph.oappw;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.vainsolutions.ph.oappw.Activities.IntroPager.IntroPagerActivity;
import com.vainsolutions.ph.oappw.Drawer.MainDrawerActivity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashScreenActivity extends Activity {
    private static String TAG = SplashScreenActivity.class.getName();
    private static long SLEEP_TIME = 2;    // Sleep for some time
    SharedPreferences sp;
    public static String file = "Files";
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        sp = getSharedPreferences(file, Context.MODE_PRIVATE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);    // Removes notification bar

        TextView apptileid = (TextView) findViewById(R.id.apptileid);
        Typeface silomFont = Typeface.createFromAsset(getAssets(), "fonts/silom.ttf");
        apptileid.setTypeface(silomFont);

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.vainsolutions.ph.oappw",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        // Start timer and launch main activity
        IntentLauncher launcher = new IntentLauncher();
        launcher.start();

    }

    private class IntentLauncher extends Thread {
        @Override
        /**
         * Sleep for some time and than start new activity.
         */
        public void run() {
            try {
                // Sleeping
                Thread.sleep(SLEEP_TIME * 1000);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }

            if (sp.getString("isfirstlunch", "").contentEquals("")) {
                Intent viewPagerIntent = new Intent(getApplicationContext(), IntroPagerActivity.class);
                startActivity(viewPagerIntent);
                SplashScreenActivity.this.finish();

            }else{
                Intent mainDrawerIntent = new Intent(getApplicationContext(),  MainDrawerActivity.class);
                startActivity(mainDrawerIntent);
                SplashScreenActivity.this.finish();
            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();



    }
    @Override
    protected void onPause() {
        super.onPause();
    }

}
