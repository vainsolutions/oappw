package com.vainsolutions.ph.oappw.Activities.Jobs;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.vainsolutions.ph.oappw.R;
import com.vainsolutions.ph.oappw.Utilities.ConnectionDetector;

public class FindMoreJobWebActivity extends ActionBarActivity {
    Boolean isInternetPresent = false;
    ConnectionDetector cd;

    WebView webview;
    ProgressBar progressBar;
    String getURL;
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.themecolor)));
        // enabling action bar app icon and behaving it as toggle button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setContentView(R.layout.jobstreet_main_layout);
        Intent i = getIntent();

        Bundle bundle = getIntent().getExtras();
        getURL = bundle.getString("keyURL");

        cd = new ConnectionDetector(FindMoreJobWebActivity.this);

        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();
        // check for Internet status
        if (isInternetPresent) {

            progressBar = (ProgressBar) findViewById(R.id.progressBar1);
            webview = (WebView) findViewById(R.id.webview);
            webview.getSettings().setJavaScriptEnabled(true);


//            webview.loadUrl("http://portal.marikina.gov.ph/business/client/home.asp");

            webview.loadUrl(getURL);


            WebSettings webSettings = webview.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webview.addJavascriptInterface(new WebAppInterface(FindMoreJobWebActivity.this), "OFW");
            webview.setWebViewClient(new BusinessPortalWebView());

            webview.getSettings().setLoadWithOverviewMode(true);
            webview.getSettings().setUseWideViewPort(true);
            webview.setInitialScale(40);
        } else {

        }

    }



    public class WebAppInterface {
        Context mContext;

        /**
         * Instantiate the interface and set the context
         */
        WebAppInterface(Context c) {
            mContext = c;
        }

        /**
         * Show a toast from the web page
         */
        @JavascriptInterface
        public void showToast(String toast) {
            Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
        }
    }

    class BusinessPortalWebView extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);


            webview.loadUrl("javascript:(function() { " + "document.getElementsByTagName('tbs-ne')[0].style.display = 'none'; " + "})()");
            progressBar.setVisibility(View.GONE);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                // app icon in action bar clicked; go home
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}