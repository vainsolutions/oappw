
package com.vainsolutions.ph.oappw.Activities.Reports;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.vainsolutions.ph.oappw.R;
import com.vainsolutions.ph.oappw.Utilities.ConnectionDetector;
import com.vainsolutions.ph.oappw.Utilities.Constant;
import com.vainsolutions.ph.oappw.Utilities.HTTP.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class AddReports extends ActionBarActivity {

    // Progress Dialog
    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();

    TextView i_reporter;
    Spinner i_report_type;
    private ArrayAdapter<String> countriesAdapter;
    AutoCompleteTextView i_country;
    EditText i_date, i_time, i_receiver_name, i_receiver_number, i_report_details;

    SharedPreferences.Editor ed;
    SharedPreferences sp;
    public static String file = "Files";
    ConnectionDetector cd;
    // JSON Node
    private static final String TAG_SUCCESS = "success";
String name;
    String ddate="";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.themecolor)));
        // enabling action bar app icon and behaving it as toggle button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setContentView(R.layout.complain_layout);
        sp = getSharedPreferences(file, 0);
        cd = new ConnectionDetector(getApplicationContext());
        i_reporter = (TextView) findViewById(R.id.i_reporter);

        ed = sp.edit();
        name = sp.getString("storedname", "");
        i_reporter.setText(name);
        i_report_type = (Spinner) findViewById(R.id.i_report_type);
        i_date = (EditText) findViewById(R.id.i_date);
        i_time = (EditText) findViewById(R.id.i_time);
        i_receiver_name = (EditText) findViewById(R.id.i_receiver_name);
        i_receiver_number = (EditText) findViewById(R.id.i_receiver_number);
        i_report_details = (EditText) findViewById(R.id.i_report_details);

        Button btnCreateProduct = (Button) findViewById(R.id.btnAddReport);

        String[] countriesArray = getResources().getStringArray(R.array.countries);
        countriesAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,countriesArray);
        i_country = (AutoCompleteTextView) findViewById(R.id.i_country);
        i_country.setAdapter(countriesAdapter);
        i_country.setThreshold(1);

        i_country.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });


        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);//starts with 0 for Jan
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "MM/dd/yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.TAIWAN);
                ddate = sdf.format(myCalendar.getTime());
                i_date.setText(ddate);
            }
        };
        i_date.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(AddReports.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        i_time.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(AddReports.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        i_time.setText( selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });

        btnCreateProduct.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // creating new report in background thread
                new CreateReport().execute();
//				new SendSMS().execute();
            }
        });
    }

    class CreateReport extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(AddReports.this);
            pDialog.setMessage("Creating Report..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {

            i_reporter = (TextView) findViewById(R.id.i_reporter);
            ed = sp.edit();
            name = sp.getString("storedname", "");
            i_reporter.setText(name);
            i_report_type = (Spinner) findViewById(R.id.i_report_type);
            i_date = (EditText) findViewById(R.id.i_date);
            i_time = (EditText) findViewById(R.id.i_time);
            i_receiver_name = (EditText) findViewById(R.id.i_receiver_name);
            i_receiver_number = (EditText) findViewById(R.id.i_receiver_number);
            i_report_details = (EditText) findViewById(R.id.i_report_details);

            String reporter = i_reporter.getText().toString();
            String report_type = i_report_type.getSelectedItem().toString();
            String country = i_country.getText().toString();
            String report_date = i_date.getText().toString();
            String report_time = i_time.getText().toString();
            String receiver_name = i_receiver_name.getText().toString();
            String receiver_number = i_receiver_number.getText().toString();
            String report_details = i_report_details.getText().toString();

            String lon="";
            String lat="";
            String report_image="";

            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("report_type", report_type));
            params.add(new BasicNameValuePair("reporter", reporter));
            params.add(new BasicNameValuePair("country", country));
            params.add(new BasicNameValuePair("report_date", report_date));
            params.add(new BasicNameValuePair("report_time", report_time));
            params.add(new BasicNameValuePair("lon", lon));
            params.add(new BasicNameValuePair("lat", lat));
            params.add(new BasicNameValuePair("receiver_name", receiver_name));
            params.add(new BasicNameValuePair("receiver_number", receiver_number));
            params.add(new BasicNameValuePair("report_details", report_details));
            params.add(new BasicNameValuePair("report_image", report_image));

            JSONObject json = jsonParser.makeHttpRequest(Constant.url_create_report,
                    "POST", params);

            // check log cat fro response
            Log.d("Create Response", json.toString());

            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully created report
                    Intent i = new Intent(getApplicationContext(), GetReports.class);
                    startActivity(i);

                    // closing this screen
                    finish();
                } else {
                    // failed to create report
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }

    class SendSMS extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(AddReports.this);
            pDialog.setMessage("Sending Message..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        protected String doInBackground(String... args) {

            i_reporter = (TextView) findViewById(R.id.i_reporter);
            i_report_type = (Spinner) findViewById(R.id.i_report_type);
            i_date = (EditText) findViewById(R.id.i_date);
            i_time = (EditText) findViewById(R.id.i_time);
            i_receiver_name = (EditText) findViewById(R.id.i_receiver_name);
            i_receiver_number = (EditText) findViewById(R.id.i_receiver_number);
            i_report_details = (EditText) findViewById(R.id.i_report_details);

            String reporter = i_reporter.getText().toString();
            String report_type = i_report_type.getSelectedItem().toString();
            String country = i_country.getText().toString();
            String report_date = i_date.getText().toString();
            String report_time = i_time.getText().toString();
            String receiver_name = i_receiver_name.getText().toString();
            String receiver_number = i_receiver_number.getText().toString();
            String report_details = i_report_details.getText().toString();

            String lon="";
            String lat="";
            String report_image="";

            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("message_type", "incoming"));
            params.add(new BasicNameValuePair("mobile_number", "639308075012"));
            params.add(new BasicNameValuePair("request_id", "123"));//don't need to reply
            params.add(new BasicNameValuePair("message", "sample message"));
            params.add(new BasicNameValuePair("timestamp", "08:00:00"));
            params.add(new BasicNameValuePair("shortcode", "2903903900"));

            JSONObject json = jsonParser.makeHttpRequest(Constant.url_sendsms,
                    "POST", params);

            // check log cat fro response
            Log.d("Create Response", json.toString());

            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully created report
                    Intent i = new Intent(getApplicationContext(), GetReports.class);
                    startActivity(i);

                    // closing this screen
                    finish();
                } else {
                    // failed to create report
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                // app icon in action bar clicked; go home
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
