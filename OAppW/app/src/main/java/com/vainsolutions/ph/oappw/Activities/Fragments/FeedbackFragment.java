/*
 * Copyright (c) 2015. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.vainsolutions.ph.oappw.Activities.Fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.vainsolutions.ph.oappw.R;
import com.vainsolutions.ph.oappw.Utilities.ConnectionDetector;
import com.vainsolutions.ph.oappw.Utilities.Constant;
import com.vainsolutions.ph.oappw.Utilities.HTTP.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class FeedbackFragment extends Fragment {

    private View parentView;

    Boolean isInternetPresent = false;
    ConnectionDetector cd;
    // Progress Dialog
    private ProgressDialog pDialog;
    // url to create new product
    JSONParser jsonParser = new JSONParser();

    public static String file = "Files";

    EditText feedbackName, feedbackEmail, feedbackMessage;
    Button sendfeedbackbt;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.feedback_layout, container, false);
        //setUpViews();

        feedbackName = (EditText) parentView.findViewById(R.id.feedbackName);
        feedbackEmail = (EditText) parentView.findViewById(R.id.feedbackEmail);
        feedbackMessage = (EditText) parentView.findViewById(R.id.feedbackMessage);

        sendfeedbackbt = (Button) parentView.findViewById(R.id.sendfeedbackbt);
        cd = new ConnectionDetector(getActivity());
        sendfeedbackbt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    String getName = feedbackName.getText().toString();
                    String getEmail = feedbackEmail.getText().toString();
                    String getMsg = feedbackMessage.getText().toString();

                    if(getName.isEmpty() || getEmail.isEmpty() || getMsg.isEmpty()){
                        new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Can't Send Feedback!")
                                .setContentText("Please fill up the form")
                                .show();
                    }else{
                        new sendFeedBack().execute();
                    }
                } else {
                    Crouton.makeText(getActivity(), "Sorry, No Internet Connection", Style.CONFIRM).show();


                    return;

                }
            }
        });

        return parentView;
    }

    class sendFeedBack extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Sending Feedback...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }
        /**
         * Send Feedback
         */
        protected String doInBackground(String... args) {

            String getName = feedbackName.getText().toString();
            String getEmail = feedbackEmail.getText().toString();
            String getMsg = feedbackMessage.getText().toString();

            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("userfeedbackname", getName));
            params.add(new BasicNameValuePair("userfeedbackemail", getEmail));
            params.add(new BasicNameValuePair("userfeedbackdesc", getMsg));

            // getting JSON Object
            // Note that create product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(Constant.FEEDBACK_URL,
                    "POST", params);

            // check log cat fro response
            Log.d("Create Response", json.toString());

            // check for success tag
            try {
                int success = json.getInt(Constant.SUCCESS);

                if (success == 1) {
                    //Intent i = new Intent(getApplicationContext(), Main.class);
                    //startActivity(i);
                    //finish();

                } else {
                    // failed
                    Toast.makeText(getActivity(), "Failed",
                            Toast.LENGTH_LONG).show();

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * *
         */
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            pDialog.dismiss();

            new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                    .setTitleText("Thank you!")
                    .setContentText("Feedback Successfully Sent")
                    .show();
            feedbackName.setText("");
            feedbackEmail.setText("");
            feedbackMessage.setText("");
        }

    }

}