/*
 * Copyright (c) 2015. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.vainsolutions.ph.oappw.Activities.Jobs;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vainsolutions.ph.oappw.R;

public class JobsCustomListMenu extends ArrayAdapter<String>{
    private final Activity context;
    private final String[] sectorListTitle;
    private final Integer[] imagesRow;
    public JobsCustomListMenu(Activity context,
                                  String[] visitorsListTitle, Integer[] imagesRow) {
        super(context, R.layout.jobs_menu_list_layout, visitorsListTitle);
        this.context = context;
        this.sectorListTitle = visitorsListTitle;
        this.imagesRow = imagesRow;
    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.jobs_menu_list_layout, null, true);
        TextView visitorsplacetitle = (TextView) rowView.findViewById(R.id.sectorTitle);
        ImageView visitorsimagelist = (ImageView) rowView.findViewById(R.id.sectorimagelist);
        visitorsimagelist.setScaleType(ImageView.ScaleType.FIT_XY);
        visitorsplacetitle.setText(sectorListTitle[position]);
        visitorsimagelist.setImageResource(imagesRow[position]);
        return rowView;
    }
}
