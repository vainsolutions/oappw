/*
 * Copyright (c) 2015. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */

package com.vainsolutions.ph.oappw.Activities.Jobs;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.vainsolutions.ph.oappw.R;
import com.vainsolutions.ph.oappw.Utilities.Constant;
import com.vainsolutions.ph.oappw.Utilities.HTTP.JSONParser;
import com.vainsolutions.ph.oappw.Utilities.ImageLoader.ImageLoader;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class JobSingleDetailView extends ActionBarActivity {
    private String jobtitle, jobdesc, jobsalary, jobavail,
            jobimage, jobcountry, joblong, joblat,
            jobsector, jobdate;
    String flag;
    ImageLoader imageLoader = new ImageLoader(this);
    public static String file = "Files";
    SharedPreferences.Editor ed;
    private ProgressDialog pDialog;
    JSONObject jsonobject;
    JSONArray jsonarray;
    ListView listview;
    JobsListViewAdapter adapter;
    ProgressDialog mProgressDialog;
    ArrayList<HashMap<String, String>> arraylist;
    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();
    SharedPreferences sp;
    ImageView calcontactimg;
    TextView marqueetext;
    String jobStreetURL;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the view from singleitemview.xml
        setContentView(R.layout.job_detail_layout);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.themecolor)));
        // enabling action bar app icon and behaving it as toggle button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        Intent i = getIntent();
        jobtitle = i.getStringExtra("jobTitle");
        jobdesc = i.getStringExtra("jobDesc");
        jobsalary = i.getStringExtra("jobSalary");

        jobavail = i.getStringExtra("jobAvailable");
        jobimage = String.valueOf(i.getByteArrayExtra("jobThumbImg"));
        jobcountry = i.getStringExtra("jobCountry");
        joblong = i.getStringExtra("jobLong");
        joblat = i.getStringExtra("jobLat");
        jobsector = i.getStringExtra("jobSector");
        jobdate = i.getStringExtra("date_added");


        // Get the result of flag
        flag = i.getStringExtra("jobThumbImg");

        // Locate the TextViews in singleitemview.xml
        TextView jobname = (TextView) findViewById(R.id.jobname);
        TextView averagesalary = (TextView) findViewById(R.id.averagesalary);
        TextView hotelrestotype = (TextView) findViewById(R.id.hotelrestotype);
        TextView descriptionRestoHotel = (TextView) findViewById(R.id.descriptionRestoHotel);

        TextView jobCountryID = (TextView) findViewById(R.id.jobCountryID);

        Typeface OmnesLight = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Thin.ttf");
        descriptionRestoHotel.setTypeface(OmnesLight);

        // Locate the ImageView in singleitemview.xml
        ImageView thumbjob = (ImageView) findViewById(R.id.thumbjob);
        thumbjob.setScaleType(ImageView.ScaleType.FIT_XY);
        // Set results to the TextViews
        jobname.setText(jobtitle);
        averagesalary.setText(jobsalary);
        hotelrestotype.setText(jobavail);
        int loader = 1;
        descriptionRestoHotel.setText(jobdesc);
        descriptionRestoHotel.setMovementMethod(new ScrollingMovementMethod());
        imageLoader.DisplayImage(flag, loader, thumbjob);

        jobCountryID.setText(jobcountry);

        jobStreetURL = Constant.JOBSTREET_API+jobtitle;
        Button findmorejobBt = (Button) findViewById(R.id.findmorejobBt);
        findmorejobBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(JobSingleDetailView.this, FindMoreJobWebActivity.class);
                intent.putExtra("keyURL", jobStreetURL);
                startActivity(intent);
            }
        });

        // Capture position and set results to the ImageView
        // Passes flag images URL into ImageLoader.class


//        http://sampleprogramz.com/android/mysqldb.php#Update Values from Android to MySQL Database
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                // app icon in action bar clicked; go home
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}